# Type Of Array Elements Sequence

## Description

Implement the `GetTypeOfElementsSequence` method, which takes an array of integers as input and determines the type of sequence:   
- strictly increasing
- strictly decreasing
- monotonic
- increasing
- decreasing
- unordered
- one element array
