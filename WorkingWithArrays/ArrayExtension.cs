﻿using System;

namespace WorkingWithArrays
{
    public static class ArrayExtension
    {
        public static string GetTypeOfElementsSequence(this int[] source) // extension method
        {
            const int one = 1;
            //_ = source is null ? throw new ArgumentNullException(nameof(source)) : source;
            // <=>
            _ = source ?? throw new ArgumentNullException(nameof(source));

            _ = source == Array.Empty<int>() ? throw new ArgumentException($"{source} is empty") : source;

            int state = 0;

            if (source.Length == one)
            {
                return "One element array.";
            }

            string typeOfSequence;
            bool lessThan = false, moreThan = false, equals = false;//1 byte
            for (int i = 0; i < source.Length - 1; i++)
            {
                if (source[i] < source[i + 1])
                {
                    lessThan = true;
                }
                else if (source[i] > source[i + 1])
                {
                    moreThan = true;
                }
                else
                {
                    equals = true;
                }
            }
            // with bool    
            // if (lessThan && !moreThan && !equals)
            // {
            //     typeOfSequence = "Strictly Increasing";
            // }
            // else if (!lessThan && moreThan && !equals)
            // {
            //     typeOfSequence = "Strictly Decreasing";
            // }
            // else if (lessThan && !moreThan && equals)
            // {
            //     typeOfSequence = "Increasing";
            // }
            // else if (!lessThan && moreThan && equals)
            // {
            //     typeOfSequence = "Decreasing";
            // }
            // else if (!lessThan && !moreThan && equals)
            // {
            //     typeOfSequence = "Monotonic";
            // }
            // else
            // {
            //     typeOfSequence = "Unordered";
            // }
            //
            // return typeOfSequence;

            // with int or byte
            // if (lessThan > 0 && moreThan == 0 && equals == 0)
            // {
            //     typeOfSequence = "Strictly Increasing";
            // }
            // else if (lessThan == 0 && moreThan > 0 && equals == 0)
            // {
            //     typeOfSequence = "Strictly Decreasing";
            // }
            // else if (lessThan > 0 && moreThan == 0 && equals > 0)
            // {
            //     typeOfSequence = "Increasing";
            // }
            // else if (lessThan == 0 && moreThan > 0 && equals > 0)
            // {
            //     typeOfSequence = "Decreasing";
            // }
            // else if (lessThan == 0 && moreThan == 0 && equals > 0)
            // {
            //     typeOfSequence = "Monotonic";
            // }
            // else
            // {
            //     typeOfSequence = "Unordered";
            // }
            //
            // return typeOfSequence;
            
            return (lessThan, moreThan, equals) switch
            {
                (true, false, false) => "Strictly Increasing",
                (false, true, false) => "Strictly Decreasing",
                (true, false, true) => "Increasing",
                (false, true, true) => "Decreasing",
                (false, false, true) => "Monotonic",
                _ => "Unordered"
            };
        }
    }
}