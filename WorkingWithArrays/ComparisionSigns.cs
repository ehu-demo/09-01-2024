﻿using System;

namespace WorkingWithArrays
{
    [Flags]
    public enum ComparisionSigns : byte
    {
        LessThan,
        MoreThan,
        Equals
    }
}